[double-linked list, public inheritance, RTTI, exceptions, move semantics]

Each Node of this double-linked List contains a copy of somehow earlier created Shape (Circle or Rect). 
General methods for a list are implemented (output/input from file, removing/adding nodes, sorting by square of shapes etc.)